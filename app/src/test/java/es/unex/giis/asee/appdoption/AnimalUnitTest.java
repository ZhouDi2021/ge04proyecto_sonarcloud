package es.unex.giis.asee.appdoption;

import static org.junit.Assert.assertEquals;


import org.junit.Test;

import java.lang.reflect.Field;
import java.util.LinkedList;
import java.util.List;

import es.unex.giis.asee.appdoption.Model.Address;
import es.unex.giis.asee.appdoption.Model.Attributes;
import es.unex.giis.asee.appdoption.Model.Breeds;
import es.unex.giis.asee.appdoption.Model.Colors;
import es.unex.giis.asee.appdoption.Model.Contact;
import es.unex.giis.asee.appdoption.Model.Environment;
import es.unex.giis.asee.appdoption.Model.Links;
import es.unex.giis.asee.appdoption.Model.Photo;
import es.unex.giis.asee.appdoption.Model.PrimaryPhotoCropped;

public class AnimalUnitTest {


    @Test
    public void setIdTest() throws NoSuchFieldException, IllegalAccessException {
        Integer value = 1;
        es.unex.giis.asee.appdoption.Model.Animal instance = new es.unex.giis.asee.appdoption.Model.Animal();
        instance.setId(value);
        final Field field = instance.getClass().getDeclaredField("id");
        field.setAccessible(true);
        assertEquals("Fields didn't match", field.get(instance), value);
    }

    @Test
    public void setOrganizationIdTest() throws NoSuchFieldException, IllegalAccessException {
        String value = "1";
        es.unex.giis.asee.appdoption.Model.Animal instance = new es.unex.giis.asee.appdoption.Model.Animal();
        instance.setOrganizationId(value);
        final Field field = instance.getClass().getDeclaredField("organizationId");
        field.setAccessible(true);
        assertEquals("Fields didn't match", field.get(instance), value);
    }

    @Test
    public void setPhotoTest() throws NoSuchFieldException, IllegalAccessException {
        String value = "animalPhoto";
        es.unex.giis.asee.appdoption.Model.Animal instance = new es.unex.giis.asee.appdoption.Model.Animal();
        instance.setPhoto(value);
        final Field field = instance.getClass().getDeclaredField("photo");
        field.setAccessible(true);
        assertEquals("Fields didn't match", field.get(instance), value);
    }

    @Test
    public void setUrlTest() throws NoSuchFieldException, IllegalAccessException {
        String value = "www.animalweb.es";
        es.unex.giis.asee.appdoption.Model.Animal instance = new es.unex.giis.asee.appdoption.Model.Animal();
        instance.setUrl(value);
        final Field field = instance.getClass().getDeclaredField("url");
        field.setAccessible(true);
        assertEquals("Fields didn't match", field.get(instance), value);
    }

    @Test
    public void setTypeTest() throws NoSuchFieldException, IllegalAccessException {
        String value = "Dog";
        es.unex.giis.asee.appdoption.Model.Animal instance = new es.unex.giis.asee.appdoption.Model.Animal();
        instance.setType(value);
        final Field field = instance.getClass().getDeclaredField("type");
        field.setAccessible(true);
        assertEquals("Fields didn't match", field.get(instance), value);
    }

    @Test
    public void setSpeciesTest() throws NoSuchFieldException, IllegalAccessException {
        String value = "Buldog";
        es.unex.giis.asee.appdoption.Model.Animal instance = new es.unex.giis.asee.appdoption.Model.Animal();
        instance.setSpecies(value);
        final Field field = instance.getClass().getDeclaredField("species");
        field.setAccessible(true);
        assertEquals("Fields didn't match", field.get(instance), value);
    }

    @Test
    public void setBreedsTest() throws NoSuchFieldException, IllegalAccessException {
        Breeds value = new Breeds();
        es.unex.giis.asee.appdoption.Model.Animal instance = new es.unex.giis.asee.appdoption.Model.Animal();
        instance.setBreeds(value);
        final Field field = instance.getClass().getDeclaredField("breeds");
        field.setAccessible(true);
        assertEquals("Fields didn't match", field.get(instance), value);
    }

    @Test
    public void setColorsTest() throws NoSuchFieldException, IllegalAccessException {
        Colors value = new Colors();
        es.unex.giis.asee.appdoption.Model.Animal instance = new es.unex.giis.asee.appdoption.Model.Animal();
        instance.setColors(value);
        final Field field = instance.getClass().getDeclaredField("colors");
        field.setAccessible(true);
        assertEquals("Fields didn't match", field.get(instance), value);
    }

    @Test
    public void setAgeTest() throws NoSuchFieldException, IllegalAccessException {
        String value = "12";
        es.unex.giis.asee.appdoption.Model.Animal instance = new es.unex.giis.asee.appdoption.Model.Animal();
        instance.setAge(value);
        final Field field = instance.getClass().getDeclaredField("age");
        field.setAccessible(true);
        assertEquals("Fields didn't match", field.get(instance), value);
    }

    @Test
    public void setGenderTest() throws NoSuchFieldException, IllegalAccessException {
        String value = "male";
        es.unex.giis.asee.appdoption.Model.Animal instance = new es.unex.giis.asee.appdoption.Model.Animal();
        instance.setGender(value);
        final Field field = instance.getClass().getDeclaredField("gender");
        field.setAccessible(true);
        assertEquals("Fields didn't match", field.get(instance), value);
    }

    @Test
    public void setSizeTest() throws NoSuchFieldException, IllegalAccessException {
        String value = "small";
        es.unex.giis.asee.appdoption.Model.Animal instance = new es.unex.giis.asee.appdoption.Model.Animal();
        instance.setSize(value);
        final Field field = instance.getClass().getDeclaredField("size");
        field.setAccessible(true);
        assertEquals("Fields didn't match", field.get(instance), value);
    }

    @Test
    public void setCoatTest() throws NoSuchFieldException, IllegalAccessException {
        String value = "none";
        es.unex.giis.asee.appdoption.Model.Animal instance = new es.unex.giis.asee.appdoption.Model.Animal();
        instance.setCoat(value);
        final Field field = instance.getClass().getDeclaredField("coat");
        field.setAccessible(true);
        assertEquals("Fields didn't match", field.get(instance), value);
    }

    @Test
    public void setAttributesTest() throws NoSuchFieldException, IllegalAccessException {
        Attributes value = new Attributes();
        es.unex.giis.asee.appdoption.Model.Animal instance = new es.unex.giis.asee.appdoption.Model.Animal();
        instance.setAttributes(value);
        final Field field = instance.getClass().getDeclaredField("attributes");
        field.setAccessible(true);
        assertEquals("Fields didn't match", field.get(instance), value);
    }

    @Test
    public void setEnvironmentTest() throws NoSuchFieldException, IllegalAccessException {
        Environment value = new Environment();
        es.unex.giis.asee.appdoption.Model.Animal instance = new es.unex.giis.asee.appdoption.Model.Animal();
        instance.setEnvironment(value);
        final Field field = instance.getClass().getDeclaredField("environment");
        field.setAccessible(true);
        assertEquals("Fields didn't match", field.get(instance), value);
    }

    @Test
    public void setTagsTest() throws NoSuchFieldException, IllegalAccessException {
        List<Object> value = new LinkedList<>();
        es.unex.giis.asee.appdoption.Model.Animal instance = new es.unex.giis.asee.appdoption.Model.Animal();
        instance.setTags(value);
        final Field field = instance.getClass().getDeclaredField("tags");
        field.setAccessible(true);
        assertEquals("Fields didn't match", field.get(instance), value);
    }

    @Test
    public void setNameTest() throws NoSuchFieldException, IllegalAccessException {
        String value = "Nicki";
        es.unex.giis.asee.appdoption.Model.Animal instance = new es.unex.giis.asee.appdoption.Model.Animal();
        instance.setName(value);
        final Field field = instance.getClass().getDeclaredField("name");
        field.setAccessible(true);
        assertEquals("Fields didn't match", field.get(instance), value);
    }

    @Test
    public void setDescriptionTest() throws NoSuchFieldException, IllegalAccessException {
        String value = "Very nice dog";
        es.unex.giis.asee.appdoption.Model.Animal instance = new es.unex.giis.asee.appdoption.Model.Animal();
        instance.setDescription(value);
        final Field field = instance.getClass().getDeclaredField("description");
        field.setAccessible(true);
        assertEquals("Fields didn't match", field.get(instance), value);
    }

    @Test
    public void setOrganizationAnimalIdTest() throws NoSuchFieldException, IllegalAccessException {
        String value = "1";
        es.unex.giis.asee.appdoption.Model.Animal instance = new es.unex.giis.asee.appdoption.Model.Animal();
        instance.setOrganizationAnimalId(value);
        final Field field = instance.getClass().getDeclaredField("organizationAnimalId");
        field.setAccessible(true);
        assertEquals("Fields didn't match", field.get(instance), value);
    }


    @Test
    public void setPhotosTest() throws NoSuchFieldException, IllegalAccessException {
        List<Photo> value = new LinkedList<>();
        es.unex.giis.asee.appdoption.Model.Animal instance = new es.unex.giis.asee.appdoption.Model.Animal();
        instance.setPhotos(value);
        final Field field = instance.getClass().getDeclaredField("photos");
        field.setAccessible(true);
        assertEquals("Fields didn't match", field.get(instance), value);
    }

    @Test
    public void setPrimaryPhotoCroppedTest() throws NoSuchFieldException, IllegalAccessException {
        PrimaryPhotoCropped value = new PrimaryPhotoCropped();
        es.unex.giis.asee.appdoption.Model.Animal instance = new es.unex.giis.asee.appdoption.Model.Animal();
        instance.setPrimaryPhotoCropped(value);
        final Field field = instance.getClass().getDeclaredField("primaryPhotoCropped");
        field.setAccessible(true);
        assertEquals("Fields didn't match", field.get(instance), value);
    }

    @Test
    public void setVideosTest() throws NoSuchFieldException, IllegalAccessException {
        List<Object> value = new LinkedList<>();
        es.unex.giis.asee.appdoption.Model.Animal instance = new es.unex.giis.asee.appdoption.Model.Animal();
        instance.setVideos(value);
        final Field field = instance.getClass().getDeclaredField("videos");
        field.setAccessible(true);
        assertEquals("Fields didn't match", field.get(instance), value);
    }

    @Test
    public void setStatusTest() throws NoSuchFieldException, IllegalAccessException {
        String value = "adoptable";
        es.unex.giis.asee.appdoption.Model.Animal instance = new es.unex.giis.asee.appdoption.Model.Animal();
        instance.setStatus(value);
        final Field field = instance.getClass().getDeclaredField("status");
        field.setAccessible(true);
        assertEquals("Fields didn't match", field.get(instance), value);
    }

    @Test
    public void setStatusChangedAtTest() throws NoSuchFieldException, IllegalAccessException {
        String value = "none";
        es.unex.giis.asee.appdoption.Model.Animal instance = new es.unex.giis.asee.appdoption.Model.Animal();
        instance.setStatusChangedAt(value);
        final Field field = instance.getClass().getDeclaredField("statusChangedAt");
        field.setAccessible(true);
        assertEquals("Fields didn't match", field.get(instance), value);
    }

    @Test
    public void setPublishedAtTest() throws NoSuchFieldException, IllegalAccessException {
        String value = "none";
        es.unex.giis.asee.appdoption.Model.Animal instance = new es.unex.giis.asee.appdoption.Model.Animal();
        instance.setPublishedAt(value);
        final Field field = instance.getClass().getDeclaredField("publishedAt");
        field.setAccessible(true);
        assertEquals("Fields didn't match", field.get(instance), value);
    }

    @Test
    public void setDistanceTest() throws NoSuchFieldException, IllegalAccessException {
        Object value = new Object();
        es.unex.giis.asee.appdoption.Model.Animal instance = new es.unex.giis.asee.appdoption.Model.Animal();
        instance.setDistance(value);
        final Field field = instance.getClass().getDeclaredField("distance");
        field.setAccessible(true);
        assertEquals("Fields didn't match", field.get(instance), value);
    }

    @Test
    public void setContactTest() throws NoSuchFieldException, IllegalAccessException {
        Address a = new Address();
        Contact value = new Contact("user@mail","9",a);
        es.unex.giis.asee.appdoption.Model.Animal instance = new es.unex.giis.asee.appdoption.Model.Animal(1,"1","Dog","Buldog","10","Male","small","Pedro","Good dog","2","adoptable","none","none",value,"none");
        instance.setContact(value);
        final Field field = instance.getClass().getDeclaredField("contact");
        field.setAccessible(true);
        assertEquals("Fields didn't match", field.get(instance), value);
    }

    @Test
    public void setLinksTest() throws NoSuchFieldException, IllegalAccessException {
        Links value = new Links();
        es.unex.giis.asee.appdoption.Model.Animal instance = new es.unex.giis.asee.appdoption.Model.Animal();
        instance.setLinks(value);
        final Field field = instance.getClass().getDeclaredField("links");
        field.setAccessible(true);
        assertEquals("Fields didn't match", field.get(instance), value);
    }

    @Test
    public void getIdTest() throws NoSuchFieldException, IllegalAccessException {
        es.unex.giis.asee.appdoption.Model.Animal instance = new es.unex.giis.asee.appdoption.Model.Animal();
        final Field field = instance.getClass().getDeclaredField("id");
        field.setAccessible(true);
        field.set(instance, 1);

        //when
        final long result = instance.getId();

        //then
        assertEquals("field wasn't retrieved properly", result, 1);
    }

    @Test
    public void getOrganizationIdTest() throws NoSuchFieldException, IllegalAccessException {
        es.unex.giis.asee.appdoption.Model.Animal instance = new es.unex.giis.asee.appdoption.Model.Animal();
        final Field field = instance.getClass().getDeclaredField("organizationId");
        field.setAccessible(true);
        field.set(instance, "2");

        //when
        final String result = instance.getOrganizationId();

        //then
        assertEquals("field wasn't retrieved properly", result, "2");
    }

    @Test
    public void getPhotoTest() throws NoSuchFieldException, IllegalAccessException {
        es.unex.giis.asee.appdoption.Model.Animal instance = new es.unex.giis.asee.appdoption.Model.Animal();
        final Field field = instance.getClass().getDeclaredField("photo");
        field.setAccessible(true);
        field.set(instance, "animalPhoto");

        //when
        final String result = instance.getPhoto();

        //then
        assertEquals("field wasn't retrieved properly", result, "animalPhoto");
    }

    @Test
    public void getUrlTest() throws NoSuchFieldException, IllegalAccessException {
        es.unex.giis.asee.appdoption.Model.Animal instance = new es.unex.giis.asee.appdoption.Model.Animal();
        final Field field = instance.getClass().getDeclaredField("url");
        field.setAccessible(true);
        field.set(instance, "www.web.es");

        //when
        final String result = instance.getUrl();

        //then
        assertEquals("field wasn't retrieved properly", result, "www.web.es");
    }

    @Test
    public void getTypeTest() throws NoSuchFieldException, IllegalAccessException {
        es.unex.giis.asee.appdoption.Model.Animal instance = new es.unex.giis.asee.appdoption.Model.Animal();
        final Field field = instance.getClass().getDeclaredField("type");
        field.setAccessible(true);
        field.set(instance, "Dog");

        //when
        final String result = instance.getType();

        //then
        assertEquals("field wasn't retrieved properly", result, "Dog");
    }

    @Test
    public void getSpeciesTest() throws NoSuchFieldException, IllegalAccessException {
        es.unex.giis.asee.appdoption.Model.Animal instance = new es.unex.giis.asee.appdoption.Model.Animal();
        final Field field = instance.getClass().getDeclaredField("species");
        field.setAccessible(true);
        field.set(instance, "Buldogg");

        //when
        final String result = instance.getSpecies();

        //then
        assertEquals("field wasn't retrieved properly", result, "Buldogg");
    }

    @Test
    public void getBreedsTest() throws NoSuchFieldException, IllegalAccessException {
        es.unex.giis.asee.appdoption.Model.Animal instance = new es.unex.giis.asee.appdoption.Model.Animal();
        final Field field = instance.getClass().getDeclaredField("breeds");
        field.setAccessible(true);
        Breeds b = new Breeds();
        field.set(instance, b);

        //when
        final Breeds result = instance.getBreeds();

        //then
        assertEquals("field wasn't retrieved properly", result, b);
    }

    @Test
    public void getColorsTest() throws NoSuchFieldException, IllegalAccessException {
        es.unex.giis.asee.appdoption.Model.Animal instance = new es.unex.giis.asee.appdoption.Model.Animal();
        final Field field = instance.getClass().getDeclaredField("colors");
        field.setAccessible(true);
        Colors c = new Colors();
        field.set(instance, c);

        //when
        final Colors result = instance.getColors();

        //then
        assertEquals("field wasn't retrieved properly", result, c);
    }

    @Test
    public void getAgeTest() throws NoSuchFieldException, IllegalAccessException {
        es.unex.giis.asee.appdoption.Model.Animal instance = new es.unex.giis.asee.appdoption.Model.Animal();
        final Field field = instance.getClass().getDeclaredField("age");
        field.setAccessible(true);
        field.set(instance, "12");

        //when
        final String result = instance.getAge();

        //then
        assertEquals("field wasn't retrieved properly", result, "12");
    }

    @Test
    public void getGenderTest() throws NoSuchFieldException, IllegalAccessException {
        es.unex.giis.asee.appdoption.Model.Animal instance = new es.unex.giis.asee.appdoption.Model.Animal();
        final Field field = instance.getClass().getDeclaredField("gender");
        field.setAccessible(true);
        field.set(instance, "male");

        //when
        final String result = instance.getGender();

        //then
        assertEquals("field wasn't retrieved properly", result, "male");
    }

    @Test
    public void getSizeTest() throws NoSuchFieldException, IllegalAccessException {
        es.unex.giis.asee.appdoption.Model.Animal instance = new es.unex.giis.asee.appdoption.Model.Animal();
        final Field field = instance.getClass().getDeclaredField("size");
        field.setAccessible(true);
        field.set(instance, "small");

        //when
        final String result = instance.getSize();

        //then
        assertEquals("field wasn't retrieved properly", result, "small");
    }

    @Test
    public void getCoatTest() throws NoSuchFieldException, IllegalAccessException {
        es.unex.giis.asee.appdoption.Model.Animal instance = new es.unex.giis.asee.appdoption.Model.Animal();
        final Field field = instance.getClass().getDeclaredField("coat");
        field.setAccessible(true);
        field.set(instance, "none");

        //when
        final String result = instance.getCoat();

        //then
        assertEquals("field wasn't retrieved properly", result, "none");
    }

    @Test
    public void getAttributesTest() throws NoSuchFieldException, IllegalAccessException {
        es.unex.giis.asee.appdoption.Model.Animal instance = new es.unex.giis.asee.appdoption.Model.Animal();
        final Field field = instance.getClass().getDeclaredField("attributes");
        field.setAccessible(true);
        Attributes a = new Attributes();
        field.set(instance, a);

        //when
        final Attributes result = instance.getAttributes();

        //then
        assertEquals("field wasn't retrieved properly", result, a);
    }

    @Test
    public void getEnvironmentTest() throws NoSuchFieldException, IllegalAccessException {
        es.unex.giis.asee.appdoption.Model.Animal instance = new es.unex.giis.asee.appdoption.Model.Animal();
        final Field field = instance.getClass().getDeclaredField("environment");
        field.setAccessible(true);
        Environment e = new Environment();
        field.set(instance, e);

        //when
        final Environment result = instance.getEnvironment();

        //then
        assertEquals("field wasn't retrieved properly", result, e);
    }

    @Test
    public void getTagsTest() throws NoSuchFieldException, IllegalAccessException {
        es.unex.giis.asee.appdoption.Model.Animal instance = new es.unex.giis.asee.appdoption.Model.Animal();
        final Field field = instance.getClass().getDeclaredField("tags");
        field.setAccessible(true);
        List<Object> value = new LinkedList<>();
        field.set(instance, value);

        //when
        final List<Object> result = instance.getTags();

        //then
        assertEquals("field wasn't retrieved properly", result, value);
    }

    @Test
    public void getNameTest() throws NoSuchFieldException, IllegalAccessException {
        es.unex.giis.asee.appdoption.Model.Animal instance = new es.unex.giis.asee.appdoption.Model.Animal();
        final Field field = instance.getClass().getDeclaredField("name");
        field.setAccessible(true);
        field.set(instance, "Nicole");

        //when
        final String result = instance.getName();

        //then
        assertEquals("field wasn't retrieved properly", result, "Nicole");
    }

    @Test
    public void getDescriptionTest() throws NoSuchFieldException, IllegalAccessException {
        es.unex.giis.asee.appdoption.Model.Animal instance = new es.unex.giis.asee.appdoption.Model.Animal();
        final Field field = instance.getClass().getDeclaredField("description");
        field.setAccessible(true);
        field.set(instance, "Nice dog");

        //when
        final String result = instance.getDescription();

        //then
        assertEquals("field wasn't retrieved properly", result, "Nice dog");
    }

    @Test
    public void getOrganizationAnimalIdTest() throws NoSuchFieldException, IllegalAccessException {
        es.unex.giis.asee.appdoption.Model.Animal instance = new es.unex.giis.asee.appdoption.Model.Animal();
        final Field field = instance.getClass().getDeclaredField("organizationAnimalId");
        field.setAccessible(true);
        field.set(instance, "1");

        //when
        final String result = instance.getOrganizationAnimalId();

        //then
        assertEquals("field wasn't retrieved properly", result, "1");
    }

    @Test
    public void getPhotosTest() throws NoSuchFieldException, IllegalAccessException {
        es.unex.giis.asee.appdoption.Model.Animal instance = new es.unex.giis.asee.appdoption.Model.Animal();
        final Field field = instance.getClass().getDeclaredField("photos");
        field.setAccessible(true);
        List<Photo> photos = new LinkedList<>();
        field.set(instance, photos);

        //when
        final List<Photo> result = instance.getPhotos();

        //then
        assertEquals("field wasn't retrieved properly", result, photos);
    }

    @Test
    public void getPrimaryPhotoCroppedTest() throws NoSuchFieldException, IllegalAccessException {
        es.unex.giis.asee.appdoption.Model.Animal instance = new es.unex.giis.asee.appdoption.Model.Animal();
        final Field field = instance.getClass().getDeclaredField("primaryPhotoCropped");
        field.setAccessible(true);
        PrimaryPhotoCropped p = new PrimaryPhotoCropped();
        field.set(instance, p);

        //when
        final PrimaryPhotoCropped result = instance.getPrimaryPhotoCropped();

        //then
        assertEquals("field wasn't retrieved properly", result, p);
    }

    @Test
    public void getVideosTest() throws NoSuchFieldException, IllegalAccessException {
        es.unex.giis.asee.appdoption.Model.Animal instance = new es.unex.giis.asee.appdoption.Model.Animal();
        final Field field = instance.getClass().getDeclaredField("videos");
        field.setAccessible(true);
        List<Object> videos = new LinkedList<>();
        field.set(instance, videos);

        //when
        final List<Object> result = instance.getVideos();

        //then
        assertEquals("field wasn't retrieved properly", result, videos);
    }

    @Test
    public void getStatusTest() throws NoSuchFieldException, IllegalAccessException {
        es.unex.giis.asee.appdoption.Model.Animal instance = new es.unex.giis.asee.appdoption.Model.Animal();
        final Field field = instance.getClass().getDeclaredField("status");
        field.setAccessible(true);
        field.set(instance, "adoptable");

        //when
        final String result = instance.getStatus();

        //then
        assertEquals("field wasn't retrieved properly", result, "adoptable");
    }

    @Test
    public void getStatusChangedAtTest() throws NoSuchFieldException, IllegalAccessException {
        es.unex.giis.asee.appdoption.Model.Animal instance = new es.unex.giis.asee.appdoption.Model.Animal();
        final Field field = instance.getClass().getDeclaredField("statusChangedAt");
        field.setAccessible(true);
        field.set(instance, "none");

        //when
        final String result = instance.getStatusChangedAt();

        //then
        assertEquals("field wasn't retrieved properly", result, "none");
    }

    @Test
    public void getPublishedAtTest() throws NoSuchFieldException, IllegalAccessException {
        es.unex.giis.asee.appdoption.Model.Animal instance = new es.unex.giis.asee.appdoption.Model.Animal();
        final Field field = instance.getClass().getDeclaredField("publishedAt");
        field.setAccessible(true);
        field.set(instance, "none");

        //when
        final String result = instance.getPublishedAt();

        //then
        assertEquals("field wasn't retrieved properly", result, "none");
    }

    @Test
    public void getDistanceTest() throws NoSuchFieldException, IllegalAccessException {
        es.unex.giis.asee.appdoption.Model.Animal instance = new es.unex.giis.asee.appdoption.Model.Animal();
        final Field field = instance.getClass().getDeclaredField("distance");
        field.setAccessible(true);
        Object distance = new Object();
        field.set(instance, distance);

        //when
        final Object result = instance.getDistance();

        //then
        assertEquals("field wasn't retrieved properly", result, distance);
    }

    @Test
    public void getContactTest() throws NoSuchFieldException, IllegalAccessException {
        es.unex.giis.asee.appdoption.Model.Animal instance = new es.unex.giis.asee.appdoption.Model.Animal();
        final Field field = instance.getClass().getDeclaredField("contact");
        field.setAccessible(true);
        Object distance = new Object();
        Address a = new Address();
        Contact c = new Contact("user@mail","9",a);
        field.set(instance, c);

        //when
        final Contact result = instance.getContact();

        //then
        assertEquals("field wasn't retrieved properly", result, c);
    }

    @Test
    public void getLinksTest() throws NoSuchFieldException, IllegalAccessException {
        es.unex.giis.asee.appdoption.Model.Animal instance = new es.unex.giis.asee.appdoption.Model.Animal();
        final Field field = instance.getClass().getDeclaredField("links");
        field.setAccessible(true);
        Links links = new Links();
        field.set(instance, links);

        //when
        final Links result = instance.getLinks();

        //then
        assertEquals("field wasn't retrieved properly", result, links);
    }


}
