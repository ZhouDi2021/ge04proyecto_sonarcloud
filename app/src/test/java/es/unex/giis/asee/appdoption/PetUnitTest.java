package es.unex.giis.asee.appdoption;

import org.junit.Test;

import java.lang.reflect.Field;

import java.util.LinkedList;
import java.util.List;

import es.unex.giis.asee.appdoption.Model.Animal;
import es.unex.giis.asee.appdoption.Model.Pet;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

public class PetUnitTest {
    @Test
    public void setAnimalTest() throws NoSuchFieldException, IllegalAccessException {
        List<Animal> value = new LinkedList<>();
        Animal animal1 = new Animal();
        Animal animal2 = new Animal();
        value.add(animal1);
        value.add(animal2);
        Pet instance = new Pet();
        instance.setAnimal(value);
        final Field field = instance.getClass().getDeclaredField("animals");
        field.setAccessible(true);
        assertEquals("Fields didn't match", field.get(instance), value);
    }

    @Test
    public void getAnimalsTest() throws NoSuchFieldException, IllegalAccessException {
        final Pet instance = new Pet();
        final Field field = instance.getClass().getDeclaredField("animals");
        field.setAccessible(true);
        List<Animal> value = new LinkedList<>();
        Animal animal1 = new Animal();
        Animal animal2 = new Animal();
        value.add(animal1);
        value.add(animal2);
        field.set(instance, value);

        //when
        final List<Animal> result = instance.getAnimals();

        //then
        assertEquals("field wasn't retrieved properly", result, value);
    }
}
