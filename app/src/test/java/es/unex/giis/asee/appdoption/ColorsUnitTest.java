package es.unex.giis.asee.appdoption;

import static org.junit.Assert.assertEquals;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import org.junit.Test;

import java.lang.reflect.Field;

public class ColorsUnitTest {


    @Test
    public void setPrimaryTest() throws NoSuchFieldException, IllegalAccessException {
        String value = "blue";
        es.unex.giis.asee.appdoption.Model.Colors instance = new es.unex.giis.asee.appdoption.Model.Colors();
        instance.setPrimary(value);
        final Field field = instance.getClass().getDeclaredField("primary");
        field.setAccessible(true);
        assertEquals("Fields didn't match", field.get(instance), value);
    }

    @Test
    public void setSecondaryTest() throws NoSuchFieldException, IllegalAccessException {
        Object value = "green";
        es.unex.giis.asee.appdoption.Model.Colors instance = new es.unex.giis.asee.appdoption.Model.Colors();
        instance.setSecondary(value);
        final Field field = instance.getClass().getDeclaredField("secondary");
        field.setAccessible(true);
        assertEquals("Fields didn't match", field.get(instance), value);
    }

    @Test
    public void setTertiaryTest() throws NoSuchFieldException, IllegalAccessException {
        Object value = "orange";
        es.unex.giis.asee.appdoption.Model.Colors instance = new es.unex.giis.asee.appdoption.Model.Colors();
        instance.setTertiary(value);
        final Field field = instance.getClass().getDeclaredField("tertiary");
        field.setAccessible(true);
        assertEquals("Fields didn't match", field.get(instance), value);
    }

    @Test
    public void getPrimaryTest() throws NoSuchFieldException, IllegalAccessException {
        es.unex.giis.asee.appdoption.Model.Colors instance = new es.unex.giis.asee.appdoption.Model.Colors();
        final Field field = instance.getClass().getDeclaredField("primary");
        field.setAccessible(true);
        field.set(instance, "blue");

        //when
        final String result = instance.getPrimary();

        //then
        assertEquals("field wasn't retrieved properly", result, "blue");
    }

    @Test
    public void getSecondaryTest() throws NoSuchFieldException, IllegalAccessException {
        es.unex.giis.asee.appdoption.Model.Colors instance = new es.unex.giis.asee.appdoption.Model.Colors();
        final Field field = instance.getClass().getDeclaredField("secondary");
        field.setAccessible(true);
        field.set(instance, "green");

        //when
        final Object result = instance.getSecondary();

        //then
        assertEquals("field wasn't retrieved properly", result, "green");
    }

    @Test
    public void getTertiaryTest() throws NoSuchFieldException, IllegalAccessException {
        es.unex.giis.asee.appdoption.Model.Colors instance = new es.unex.giis.asee.appdoption.Model.Colors();
        final Field field = instance.getClass().getDeclaredField("tertiary");
        field.setAccessible(true);
        field.set(instance, "orange");

        //when
        final Object result = instance.getTertiary();

        //then
        assertEquals("field wasn't retrieved properly", result, "orange");
    }
}
