package es.unex.giis.asee.appdoption.ui.crud_upd_del;

import android.content.Context;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.navigation.NavDirections;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;

import com.google.android.material.snackbar.Snackbar;
import com.google.android.material.textfield.TextInputEditText;

import es.unex.giis.asee.appdoption.AppExecutors;
import es.unex.giis.asee.appdoption.DependencyInjectors.AppContainer;
import es.unex.giis.asee.appdoption.DependencyInjectors.MyApplication;
import es.unex.giis.asee.appdoption.Model.Animal;
import es.unex.giis.asee.appdoption.Model.Breeds;
import es.unex.giis.asee.appdoption.Navigate;
import es.unex.giis.asee.appdoption.R;
import es.unex.giis.asee.appdoption.ViewModels.UpdDelFragmentViewModel;
import es.unex.giis.asee.appdoption.roomdb.AppDoptionDatabase;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link UpdDelFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class UpdDelFragment extends Fragment {

    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;
    private View mView;
    private Context context;

    public UpdDelFragment() {
        // Required empty public constructor
    }

    @Override
    public void onAttach(Context context){
        super.onAttach(context);
        this.context=context;
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment UpdDelFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static UpdDelFragment newInstance(String param1, String param2) {
        UpdDelFragment fragment = new UpdDelFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        mView = inflater.inflate(R.layout.fragment_upd_del, container, false);

        AppContainer appContainer = ((MyApplication)getActivity().getApplication()).appContainer;
        UpdDelFragmentViewModel vModel = new ViewModelProvider(this,appContainer.updelFactory).get(UpdDelFragmentViewModel.class);

        Animal a = (Animal)getArguments().getSerializable("data");

        TextView name = (TextView)mView.findViewById(R.id.animal_name_delete);
        name.setText(a.getName());

        TextView breed = (TextView)mView.findViewById(R.id.animal_breed_delete);
        if(a.getBreeds()!=null) {
            breed.setText(a.getBreeds().getPrimary());
        }

        TextView age = (TextView)mView.findViewById(R.id.animal_age_delete);
        age.setText(a.getAge());

        TextView estado = (TextView)mView.findViewById(R.id.animal_state_delete);
        estado.setText(a.getStatus());


        TextInputEditText description = (TextInputEditText) mView.findViewById(R.id.description_text);
        description.setText(a.getDescription());



        Button cancelBtn = (Button) mView.findViewById(R.id.cancel_btn);
        cancelBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                NavDirections action = UpdDelFragmentDirections.actionUpdDelPetToNavMypets();
                ((Navigate)context).navigateTo(action);
            }
        });

        ImageButton deleteBtn = (ImageButton) mView.findViewById(R.id.deleteBtn);
        deleteBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                vModel.deleteAnimal(a.getId());
                Snackbar.make(mView,"La mascota "+a.getName()+" ha sido eliminada",Snackbar.LENGTH_SHORT).show();
                NavDirections action = UpdDelFragmentDirections.actionUpdDelPetToNavMypets();
                ((Navigate)context).navigateTo(action);
            }
        });


        Button mdfyBtn = (Button) mView.findViewById(R.id.send_btn);
        mdfyBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                a.setName(name.getText().toString());
                a.setAge(age.getText().toString());
                Breeds breeds = new Breeds();
                breeds.setPrimary(breed.getText().toString());
                a.setBreeds(breeds);
                a.setStatus(estado.getText().toString());
                a.setDescription(description.getText().toString());

                //REALIZAR VIEW MODEL
                vModel.updateAnimal(a);
                Snackbar.make(mView,"La mascota "+a.getName()+" ha sido modificada",Snackbar.LENGTH_SHORT).show();
                NavDirections action = UpdDelFragmentDirections.actionUpdDelPetToNavMypets();
                ((Navigate)context).navigateTo(action);
            }
        });


        return mView;
    }
}