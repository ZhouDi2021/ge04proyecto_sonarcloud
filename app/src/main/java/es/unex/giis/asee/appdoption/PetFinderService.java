package es.unex.giis.asee.appdoption;

import es.unex.giis.asee.appdoption.Model.Auth;
import es.unex.giis.asee.appdoption.Model.Pet;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import retrofit2.http.Query;

public interface PetFinderService {
    @POST("v2/oauth2/token")
    @FormUrlEncoded
    Call<Auth> getAuth(
            @Field("grant_type")String grant,
            @Field("client_id") String clientId,
            @Field("client_secret") String secret);

    @Headers({"Content-Type: application/json;charset=UTF-8"})
    @GET("v2/animals?")
    Call<Pet> listPets(@Header("Authorization") String token,@Query("limit") int limit,@Query("status") String status);
}
