package es.unex.giis.asee.appdoption.datamanagement;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import java.util.List;

import es.unex.giis.asee.appdoption.AppExecutors;
import es.unex.giis.asee.appdoption.Model.Animal;
import es.unex.giis.asee.appdoption.PetsNetworkLoaderRunnable;


public class PetNetworkDataSource {
    public static PetNetworkDataSource sInstance;

    private final MutableLiveData<List<Animal>> mDownloadedAnimals;



    public PetNetworkDataSource () {
        this.mDownloadedAnimals = new MutableLiveData<>();
    }

    public static PetNetworkDataSource getInstance(){
        if(sInstance==null){
            sInstance = new PetNetworkDataSource();
        }
        return sInstance;
    }

    public LiveData<List<Animal>> getCurrentAnimals(){return mDownloadedAnimals;}

    public void fetchAnimals(){
        AppExecutors.getInstance().networkIO().execute(new PetsNetworkLoaderRunnable(animals -> mDownloadedAnimals.postValue(animals)));
    }



}
