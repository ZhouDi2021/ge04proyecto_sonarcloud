package es.unex.giis.asee.appdoption.Factories;

import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;

import es.unex.giis.asee.appdoption.ViewModels.FavouriteFragmentViewModel;
import es.unex.giis.asee.appdoption.datamanagement.PetsRepository;

public class FavouriteCRUDViewModelFragmentFactory extends ViewModelProvider.NewInstanceFactory {
    private final PetsRepository mRepository;

    public FavouriteCRUDViewModelFragmentFactory(PetsRepository mRepository) {
        this.mRepository = mRepository;
    }

    @Override
    public <T extends ViewModel> T create(Class<T> modelClass) {
        //noinspection unchecked
        return (T) new FavouriteFragmentViewModel(mRepository);
    }
}
