package es.unex.giis.asee.appdoption;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;

import es.unex.giis.asee.appdoption.ui.contact.ContactFragment;

public class ContactActivityTest extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_contact_test);
        ContactFragment fragment = new ContactFragment();
        Bundle bundle = new Bundle();
        bundle.putString("email_contact","pruebaArg@gmail.com");
        fragment.setArguments(bundle);
        getSupportFragmentManager().beginTransaction().add(R.id.contactTest, fragment).commit();

    }
}