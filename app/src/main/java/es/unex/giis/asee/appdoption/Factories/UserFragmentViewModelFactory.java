package es.unex.giis.asee.appdoption.Factories;

import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;

import es.unex.giis.asee.appdoption.ViewModels.UserFragmentViewModel;
import es.unex.giis.asee.appdoption.datamanagement.PetsRepository;


public class UserFragmentViewModelFactory extends ViewModelProvider.NewInstanceFactory {
    private final PetsRepository mRepository;

    public UserFragmentViewModelFactory(PetsRepository repository) {
        this.mRepository = repository;
    }
    @Override
    public <T extends ViewModel> T create(Class<T> modelClass) {
        //noinspection unchecked
        return (T) new UserFragmentViewModel(mRepository);
    }
}
