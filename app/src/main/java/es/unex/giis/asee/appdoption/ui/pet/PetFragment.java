package es.unex.giis.asee.appdoption.ui.pet;

import android.content.Context;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.navigation.NavDirections;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.Iterator;

import com.google.android.material.snackbar.Snackbar;


import es.unex.giis.asee.appdoption.AppExecutors;
import es.unex.giis.asee.appdoption.DependencyInjectors.AppContainer;
import es.unex.giis.asee.appdoption.DependencyInjectors.MyApplication;
import es.unex.giis.asee.appdoption.Model.Address;
import es.unex.giis.asee.appdoption.Model.Contact;
import es.unex.giis.asee.appdoption.Model.Photo;
import es.unex.giis.asee.appdoption.Model.Animal;
import es.unex.giis.asee.appdoption.Model.Favorite;
import es.unex.giis.asee.appdoption.Model.User;
import es.unex.giis.asee.appdoption.Navigate;
import es.unex.giis.asee.appdoption.R;
import es.unex.giis.asee.appdoption.ViewModels.FavouriteFragmentViewModel;
import es.unex.giis.asee.appdoption.roomdb.AppDoptionDatabase;


/**
 * A simple {@link Fragment} subclass.
 * Use the {@link PetFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class PetFragment extends Fragment {

    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;
    private View mView;
    private Context context;

    public PetFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment PetFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static PetFragment newInstance(String param1, String param2) {
        PetFragment fragment = new PetFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onAttach(Context context){
        super.onAttach(context);
        this.context=context;
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        mView = inflater.inflate(R.layout.fragment_pet, container, false);

        AppContainer appContainer = ((MyApplication)getActivity().getApplication()).appContainer;
        FavouriteFragmentViewModel fvModel = new ViewModelProvider(this,appContainer.favPetFactory).get(FavouriteFragmentViewModel.class);

        Animal a = (Animal) getArguments().getSerializable("data");

        if(a.getContact()==null){
            Address address = new Address("Refugio","Caceres","Extremadura","10002","ES");
            Contact contact = new Contact("default@gmail.com","654738222",address);
            a.setContact(contact);
        }

        ImageView imgView = mView.findViewById(R.id.petImage);
        TextView petName = mView.findViewById(R.id.nombre_mascota);
        TextView petBreed = mView.findViewById(R.id.raza_mascota);
        TextView petAge = mView.findViewById(R.id.edad_mascota);
        TextView petStatus = mView.findViewById(R.id.estado_mascota);
        TextView petDescription = mView.findViewById(R.id.descripcion_mascota);

        if (a.getPhoto() == null) {
            if (a.getPhotos() != null) {

                Iterator<Photo> it = a.getPhotos().iterator();
                if (it.hasNext()) a.setPhoto(it.next().getLarge());
            } else {
                a.setPhoto(null);
            }
        }

        if (a.getPhoto() != null) {
            Picasso.with(getContext()).load(a.getPhoto()).error(R.drawable.sample).into(imgView);
        } else {
            Picasso.with(getContext()).load(R.drawable.sample).into(imgView);
        }
        petName.setText(a.getName());

        if (a.getBreeds() != null) {
            petBreed.setText(a.getBreeds().getPrimary());
        }
        petAge.setText(a.getAge());
        if (a.getStatus() != null) {
            petStatus.setText(a.getStatus());
        }
        if(a.getDescription()==null){
            petDescription.setText("El animal no tiene descripción, pero es muy adorable ;) .");
        }else{
            petDescription.setText(a.getDescription());
        }

        Button btnAdopt = (Button) mView.findViewById(R.id.cancel_btn);
        btnAdopt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                NavDirections action = PetFragmentDirections.actionNavPetToNavContact(a.getContact().getEmail());
                ((Navigate)context).navigateTo(action);
            }
        });

        ImageButton favBtn = (ImageButton) mView.findViewById(R.id.btnFav);
        favBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if(fvModel.hasFav(a.getId())==0){
                    Favorite fav = new Favorite(0, a);
                    fvModel.addFavorite(fav);
                    Snackbar.make(mView,"Mascota añadida a Mis Favoritos",Snackbar.LENGTH_SHORT).show();
                }
                else{
                    fvModel.deleteFavorite(a.getId());
                    Snackbar.make(mView,"Mascota eliminada de Mis Favoritos",Snackbar.LENGTH_SHORT).show();
                }
            }
        });
        return mView;
    }
}