package es.unex.giis.asee.appdoption.ViewModels;

import androidx.lifecycle.ViewModel;

import es.unex.giis.asee.appdoption.Model.Favorite;
import es.unex.giis.asee.appdoption.datamanagement.PetsRepository;


public class FavouriteFragmentViewModel extends ViewModel {
    private final PetsRepository mRepository;

    public FavouriteFragmentViewModel(PetsRepository mRepository) {
        this.mRepository = mRepository;
    }

    public int hasFav(long aid){
        return mRepository.getFav(aid);
    }

    public void addFavorite(Favorite fav){
        mRepository.insertFavourite(fav);
    }

    public void deleteFavorite(long aid){
        mRepository.deleteFavourite(aid);
    }
}
