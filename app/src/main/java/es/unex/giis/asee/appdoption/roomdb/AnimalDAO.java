package es.unex.giis.asee.appdoption.roomdb;

import static androidx.room.OnConflictStrategy.REPLACE;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import java.util.List;

import es.unex.giis.asee.appdoption.Model.Animal;


@Dao
public interface AnimalDAO {

    @Insert(onConflict = REPLACE)
    void bulkInsert(List<Animal> animals);

    @Insert
    public long insert(Animal animal);

    @Update
    int update(Animal animal);

    @Query("SELECT COUNT(*) FROM Animal")
    int getNumberTotalAnimals();

    @Query("SELECT * FROM Animal WHERE email = :owner_email")
    LiveData<List<Animal>> getUserAnimals(String owner_email);

    @Query("DELETE FROM Animal WHERE id=:id")
    void delete(long id);

    @Query("UPDATE Animal SET email = :unemail WHERE email = :uoldmail")
    void updateAll(String unemail,String uoldmail);

    @Query("DELETE FROM Animal WHERE email <> :user_mail")
    void deleteAll(String user_mail);

    @Query("DELETE FROM Animal")
    void deleteAllForTest();
}
