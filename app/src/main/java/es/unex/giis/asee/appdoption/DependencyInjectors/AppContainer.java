package es.unex.giis.asee.appdoption.DependencyInjectors;

import android.content.Context;

import es.unex.giis.asee.appdoption.AppExecutors;
import es.unex.giis.asee.appdoption.Factories.CreatePetViewModelFactory;
import es.unex.giis.asee.appdoption.Factories.FavouriteCRUDViewModelFragmentFactory;
import es.unex.giis.asee.appdoption.Factories.FavouritesViewModelFactory;
import es.unex.giis.asee.appdoption.Factories.HomeViewModelFactory;
import es.unex.giis.asee.appdoption.Factories.MyPetsHomeViewModelFactory;
import es.unex.giis.asee.appdoption.Factories.UpdDelFragmentViewModelFactory;
import es.unex.giis.asee.appdoption.Factories.UserFragmentViewModelFactory;
import es.unex.giis.asee.appdoption.Model.Address;
import es.unex.giis.asee.appdoption.Model.User;
import es.unex.giis.asee.appdoption.datamanagement.PetNetworkDataSource;
import es.unex.giis.asee.appdoption.datamanagement.PetsRepository;
import es.unex.giis.asee.appdoption.roomdb.AppDoptionDatabase;


public class AppContainer {

    private AppDoptionDatabase database;
    private PetNetworkDataSource networkDataSource;
    public PetsRepository repository;
    public HomeViewModelFactory homefactory;
    public MyPetsHomeViewModelFactory userPetsFactory;
    public FavouritesViewModelFactory favUserFactory;
    public CreatePetViewModelFactory createPetFactory;
    public UpdDelFragmentViewModelFactory updelFactory;
    public FavouriteCRUDViewModelFragmentFactory favPetFactory;
    public UserFragmentViewModelFactory userFactory;


    //CREACION DE DEPENDENCIAS NECESARIAS PARA EL FUNCIONAMIENTO DE APPDOPTION
    public AppContainer(Context context){
        database = AppDoptionDatabase.getInstace(context);

        //CREACION DE USUARIO LOGUEADO
        Address address=new Address("Escuela Politecnica","Caceres", "Caceres","10002","Caceres");

        User user = new User("Roberto","admin@admin.com","12345","564923289",address);

        AppExecutors.getInstance().diskIO().execute(new Runnable() {
            @Override
            public void run() {
                if(database.getUserDAO().getUser()==null)
                    database.getUserDAO().insert(user);
            }
        });
        networkDataSource = PetNetworkDataSource.getInstance();
        repository = PetsRepository.getInstance(database.getAnimalDAO(), database.getFavDAO(), networkDataSource, context);
        homefactory = new HomeViewModelFactory(repository);
        userPetsFactory = new MyPetsHomeViewModelFactory(repository);
        favUserFactory = new FavouritesViewModelFactory(repository);
        createPetFactory = new CreatePetViewModelFactory(repository);
        updelFactory=new UpdDelFragmentViewModelFactory(repository);
        favPetFactory = new FavouriteCRUDViewModelFragmentFactory(repository);
        userFactory=new UserFragmentViewModelFactory(repository);
    }


}
