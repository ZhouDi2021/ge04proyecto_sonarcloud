package es.unex.giis.asee.appdoption;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;

import es.unex.giis.asee.appdoption.ui.crud_upd_del.UpdDelFragment;

public class DeleteTestActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_delete_test);
        UpdDelFragment fragment = new UpdDelFragment();
        getSupportFragmentManager().beginTransaction().add(R.id.deleteTest, fragment).commit();

    }
}