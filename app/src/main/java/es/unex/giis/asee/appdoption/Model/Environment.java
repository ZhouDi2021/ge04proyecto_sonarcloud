
package es.unex.giis.asee.appdoption.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class Environment implements Serializable
{

    @SerializedName("children")
    @Expose
    private Object children;
    @SerializedName("dogs")
    @Expose
    private Object dogs;
    @SerializedName("cats")
    @Expose
    private Object cats;
    private final static long serialVersionUID = -5585609468352036610L;

    public Object getChildren() {
        return children;
    }

    public void setChildren(Object children) {
        this.children = children;
    }

    public Object getDogs() {
        return dogs;
    }

    public void setDogs(Object dogs) {
        this.dogs = dogs;
    }

    public Object getCats() {
        return cats;
    }

    public void setCats(Object cats) {
        this.cats = cats;
    }

}
