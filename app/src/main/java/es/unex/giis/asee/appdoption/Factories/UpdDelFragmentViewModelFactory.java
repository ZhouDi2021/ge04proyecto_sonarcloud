package es.unex.giis.asee.appdoption.Factories;

import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;

import es.unex.giis.asee.appdoption.ViewModels.UpdDelFragmentViewModel;
import es.unex.giis.asee.appdoption.datamanagement.PetsRepository;


public class UpdDelFragmentViewModelFactory extends ViewModelProvider.NewInstanceFactory {

    private final PetsRepository mRepository;

    public UpdDelFragmentViewModelFactory(PetsRepository repository){
        this.mRepository=repository;
    }

    @Override
    public <T extends ViewModel> T create(Class<T> modelClass) {
        //noinspection unchecked
        return (T) new UpdDelFragmentViewModel(mRepository);
    }
}
