package es.unex.giis.asee.appdoption;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;

import es.unex.giis.asee.appdoption.ui.pet.PetFragment;

public class DetailTestActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_test);
        PetFragment fragment = new PetFragment();
        getSupportFragmentManager().beginTransaction().add(R.id.detail_test, fragment).commit();
    }
}