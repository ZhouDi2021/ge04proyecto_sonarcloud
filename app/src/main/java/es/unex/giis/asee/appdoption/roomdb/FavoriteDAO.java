package es.unex.giis.asee.appdoption.roomdb;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.Query;

import java.util.List;

import es.unex.giis.asee.appdoption.Model.Animal;
import es.unex.giis.asee.appdoption.Model.Favorite;
import es.unex.giis.asee.appdoption.Model.Favorite;

@Dao
public interface FavoriteDAO {
    @Insert
    public long insert(Favorite fav);

    @Query("DELETE FROM FAVORITES WHERE userid = :uid AND id = :aid")
    public void delete(long uid, long aid);

    @Query("SELECT * FROM favorites WHERE userid = :uid")
    public LiveData<List<Favorite>> getAllFavs(long uid);

    @Query("SELECT COUNT(*) FROM favorites WHERE userid = :uid AND id = :aid")
    int getFav(long uid, long aid);
    @Query("DELETE FROM favorites")
    public void deleteAllForTest();
}
