package es.unex.giis.asee.appdoption;

import java.util.List;

import es.unex.giis.asee.appdoption.Model.Animal;


public interface OnPetsLoadedListener {
    public void onPetsLoaded(List<Animal> pets);
}
